# README #

Be able to compile gpsfish with android SDK
.
Reference
http://gps.tanaka.ecc.u-tokyo.ac.jp/gpsshogi/index.php?GPSFish

### What is this repository for? ###

* gpsfish for Android

### How do I get set up? ###

Reference
http://gps.tanaka.ecc.u-tokyo.ac.jp/gpsshogi/index.php?GPSFish

### License ###
Available under GPL version 3.
元のstockfish同様GPL version 3で公開します
詳細は、元のライセンスをお読みください。(このガイドが間違っていたとしても、元のライセンスが優先します)
Please refer to original page.
http://gps.tanaka.ecc.u-tokyo.ac.jp/gpsshogi/index.php?GPSFish

### Original Source ###
http://gps.tanaka.ecc.u-tokyo.ac.jp/cgi-bin/viewvc.cgi/branches/gpsfish_dev/?root=gpsfish

File  	Rev.	Age	Author	Last log entry
  Parent Directory	 	 	 	 
  data/	 27	 3 years	 kaneko	 correction of previous commit
  sample/	 55	 2 years	 daigo	 Updates for Windows build.
  src/	 12130	 20 months	 kaneko	 workaround for an internal compile error when building gpsfishone
  AUTHORS.gpsfish	 24	 3 years	 daigo	 Added files needed to build Windows package.
  AUTHORS.stockfish	 24	 3 years	 daigo	 Added files needed to build Windows package.
  CMakeLists.txt	 55	 2 years	 daigo	 Updates for Windows build.
  Copying.txt	 8	 3 years	 ktanaka	 
  Readme.txt	 8	 3 years	 ktanaka